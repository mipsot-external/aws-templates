# Usage

## Cloudformation templates

1. `cloudformation/ec2-ubuntu-cfn.template.yml`:
```yaml
# ./template.yml
<...>
Resources:
  EC2UbuntuStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: /tmp/packaged.ec2-ubuntu-cfn.template.yml
      Parameters:
        InstanceType: t2.micro
        # hard code to prevent recreating when AMI id will update
        InstanceImageId: !Ref AmiImageId
        InstanceTags: !Join
        - ','
        - - !Sub Name=${InstanceName}
          - !Sub Environment=${Environment}
        KeyName: development
<...>
Outputs:
  InstanceId:
    Description: InstanceId of the newly created EC2 instance
    Value: !GetAtt EC2UbuntuStack.Outputs.InstanceId
  AZ:
    Description: Availability Zone of the newly created EC2 instance
    Value: !GetAtt EC2UbuntuStack.Outputs.AZ
```
```bash
# ./deploy.sh
set -eux
curl --fail --silent --show-error \
  --output /tmp/packaged.ec2-ubuntu-cfn.template.yml \
  https://gitlab.com/mipsot-external/aws-templates/-/raw/master/cloudformation/ec2-ubuntu-cfn.template.yml
aws cloudformation package \
  --template-file template.yml \
  --s3-bucket cf-templates-deploy \
  --output-template-file /tmp/packaged.cloudformation.template.yml
aws cloudformation deploy \
  --no-fail-on-empty-changeset \
  --stack-name cloudformation-stack \
  --template-file /tmp/packaged.cloudformation.template.yml
rm -v /tmp/packaged.*.yml
```
