# Resources:
#   TargetGroup:
#     Type: AWS::ElasticLoadBalancingV2::TargetGroup
#     Properties:
#         Name: !Sub ${AWS::StackName}-tg
#       Protocol: TCP
#       Port: 80
#   AutoScalingGroup:
#     Type: AWS::AutoScaling::AutoScalingGroup
#     CreationPolicy:
#       ResourceSignal: {}
#     Properties:
#       TargetGroupARNs:
#       - !Ref TargetGroup
#       AvailabilityZones: !GetAZs ''
#       LaunchConfigurationName: !GetAtt LaunchConfigStack.Outputs.LaunchConfig
#       MinSize: '1'
#       MaxSize: '5'
#   LaunchConfigStack:
#     Type: AWS::CloudFormation::Stack
#     Properties:
#       TemplateURL: ./launchconfig-ubuntu-cfn.template.yml
#       Parameters:
#         SignalAcceptor: !Sub ${AWS::StackName}.AutoScalingGroup
#         InstanceType: t2.micro
#         InstanceTags: Name=example,Environment=development
#         KeyName: development
---
AWSTemplateFormatVersion: 2010-09-09
Description: Create a launch config with Ubuntu OS and auto updatable cfn-hup service
Parameters:
  SignalAcceptor:
    Type: String
    Description: |
      Logical resource name: ${StackName}.${ResourceLogicalName}
    Default: ''
    AllowedPattern: >-
      (\w[\w\d-]{0,127}\.[\w\d]{0,255})?
  InstanceType:
    Type: String
    Description: EC2 instance type
    AllowedPattern: >-
      (t|m|c|g|r|i|d|hi|hs|cr|cc|cg)[1-4]a?\.(nano|micro|small|medium|((|2|4|8|10)x)?large)
    ConstraintDescription: Must be a valid EC2 instance type
  InstanceImageId:
    Type: String
    Description: Custom instance AMI id (override InstanceOsVersion)
    Default: ''
    AllowedPattern: (ami-[0-9a-fA-F]+)?
    ConstraintDescription: Must be a valid AMI id
  InstanceOsVersion:
    Type: String
    Description: Version of the debian-based OS
    Default: ubuntu20
    AllowedValues:
    - ubuntu18
    - ubuntu20
    ConstraintDescription: Must be a valid debian-based OS with version
  Monitoring:
    Type: String
    Default: ''
    AllowedValues:
    - ''
    - 'true'
    - 'false'
  SshUsernameAliases:  # For unificating multiple hosts ~/.ssh/config
    Type: String
    Default: ''
    AllowedPattern: (([a-z_][a-z0-9_-]{0,31})(,[a-z_][a-z0-9_-]{0,31})*)?
    ConstraintDescription: Must be a comma delimited username aliases
  RootDeviceName:
    Type: String
    Description: Instance root device name (need for RootVolumeSize and root volume tags)
    Default: /dev/sda1
    AllowedValues:
    - /dev/sda1
    - /dev/xvda1
    - /dev/nvme0n1p1
  RootVolumeSize:
    Type: Number
    Description: Custom instance root volume size (in GiB)
    Default: 0
  InstanceTags:
    Type: String
    Description: Comma delimited list of EC2 tags
    Default: ''
    AllowedPattern: ([^=]+=([^,]+)?(,[^=]+=([^,]+)?)*)?
    ConstraintDescription: Must be a 'key=value' list
  RootVolumeTags:
    Type: String
    Description: Comma delimited list of EC2 root volume tags
    Default: ''
    AllowedPattern: ([^=]+=[^,]+(,[^=]+=[^,]+)*)?
    ConstraintDescription: Must be a 'key=value' list
  IamInstanceProfileArn:
    Type: String
    Description: Custom instance profile Arn
    Default: ''
  KeyName:
    Type: String
    Description: EC2 instance key name
    Default: ''
  AdditionalAuthorizedKeys:
    Type: String
    Default: ''
  KeepAliveTime:
    Type: Number
    Description: Prevent disconnects (0 to do nothing)
    Default: 300
  AssociatePublicIpAddress:
    Type: String
    Default: ''
    AllowedValues:
    - ''
    - 'true'
    - 'false'
  SecurityGroups:
    Type: CommaDelimitedList
    Default: ''
    ConstraintDescription: Must be a list of EC2 security group ids
  ActivateAmazonTimeSyncService:
    Type: String
    Default: 'true'
    AllowedValues:
    - 'true'
    - 'false'
  CfnHupVerbose:
    Type: String
    Default: 'true'
    AllowedValues:
    - 'true'
    - 'false'
  CfnHupTimer:
    Type: String
    Default: '5min'
    Description: Reloader timeout in systemd timer format
  CustomInitScript:
    Type: String
    Default: ''
  CustomUpdateScript:
    Type: String
    Default: ''
  CustomInitUpdateScript:
    Type: String
    Default: ''
Conditions:
  UseSignalAcceptor: !Not
  - !Equals
    - !Ref SignalAcceptor
    - ''
  UseCustomMonitoring: !Not
  - !Equals
    - !Ref Monitoring
    - ''
  UseInstanceTags: !Not
  - !Equals
    - !Ref InstanceTags
    - ''
  UseRootVolumeTags: !Not
  - !Equals
    - !Ref RootVolumeTags
    - ''
  UseTags: !Or
  - !Condition UseInstanceTags
  - !Condition UseRootVolumeTags
  UseSshUsernameAliases: !Not
  - !Equals
    - !Ref SshUsernameAliases
    - ''
  UseExternalInstanceProfile: !Not
  - !Equals
    - !Ref IamInstanceProfileArn
    - ''
  CreateInstanceProfile: !And
  - !Condition UseTags
  - !Equals
    - !Ref IamInstanceProfileArn
    - ''
  AssignInstanceKeyName: !Not
  - !Equals
    - !Ref KeyName
    - ''
  UseAdditionalAuthorizedKeys: !Not
  - !Equals
    - !Ref AdditionalAuthorizedKeys
    - ''
  UseKeepAliveTime: !Not
  - !Equals
    - !Ref KeepAliveTime
    - 0
  UseCustomAssociatePublicIpAddress: !Not
  - !Equals
    - !Ref AssociatePublicIpAddress
    - ''
  UseAssociatePublicIpAddress: !Equals
  - !Ref AssociatePublicIpAddress
  - 'true'
  UseCustomSecurityGroups: !Not
  - !Equals
    - !Join
      - ''
      - !Ref SecurityGroups
    - ''
  UseInstanceImageId: !Not
  - !Equals
    - !Ref InstanceImageId
    - ''
  UseRootVolumeSize: !Not
  - !Equals
    - !Ref RootVolumeSize
    - 0
  UseActivateAmazonTimeSyncService: !Equals
  - !Ref ActivateAmazonTimeSyncService
  - 'true'
Mappings:
  AWSRegion2AMI:
    us-east-1:
      regiondesc: US East (N. Virginia)
      ubuntu18: ami-0747bdcabd34c712a
      ubuntu20: ami-09e67e426f25ce0d7
    us-east-2:
      regiondesc: US East (Ohio)
      ubuntu18: ami-0b9064170e32bde34
      ubuntu20: ami-00399ec92321828f5
    us-west-1:
      regiondesc: US West (N. California)
      ubuntu18: ami-07b068f843ec78e72
      ubuntu20: ami-0d382e80be7ffdae5
    us-west-2:
      regiondesc: US West (Oregon)
      ubuntu18: ami-090717c950a5c34d3
      ubuntu20: ami-03d5c68bab01f3496
    ap-south-1:
      regiondesc: Asia Pacific (Mumbai)
      ubuntu18: ami-04bde106886a53080
      ubuntu20: ami-0c1a7f89451184c8b
    ap-northeast-3:
      regiondesc: Asia Pacific (Osaka)
      ubuntu18: ami-092faff259afb9a26
      ubuntu20: ami-0001d1dd884af8872
    ap-northeast-2:
      regiondesc: Asia Pacific (Seoul)
      ubuntu18: ami-0ba5cd124d7a79612
      ubuntu20: ami-04876f29fd3a5e8ba
    ap-southeast-1:
      regiondesc: Asia Pacific (Singapore)
      ubuntu18: ami-055147723b7bca09a
      ubuntu20: ami-0d058fe428540cd89
    ap-southeast-2:
      regiondesc: Asia Pacific (Sydney)
      ubuntu18: ami-0f39d06d145e9bb63
      ubuntu20: ami-0567f647e75c7bc05
    ap-northeast-1:
      regiondesc: Asia Pacific (Tokyo)
      ubuntu18: ami-0fe22bffdec36361c
      ubuntu20: ami-0df99b3a8349462c6
    ca-central-1:
      regiondesc: Canada (Central)
      ubuntu18: ami-0e28822503eeedddc
      ubuntu20: ami-0801628222e2e96d6
    eu-central-1:
      regiondesc: Europe (Frankfurt)
      ubuntu18: ami-0b1deee75235aa4bb
      ubuntu20: ami-05f7491af5eef733a
    eu-west-1:
      regiondesc: Europe (Ireland)
      ubuntu18: ami-0943382e114f188e8
      ubuntu20: ami-0a8e758f5e873d1c1
    eu-west-2:
      regiondesc: Europe (London)
      ubuntu18: ami-09a56048b08f94cdf
      ubuntu20: ami-0194c3e07668a7e36
    eu-west-3:
      regiondesc: Europe (Paris)
      ubuntu18: ami-06602da18c878f98d
      ubuntu20: ami-0f7cd40eac2214b37
    eu-north-1:
      regiondesc: Europe (Stockholm)
      ubuntu18: ami-0afad43e7d620260c
      ubuntu20: ami-0ff338189efb7ed37
    sa-east-1:
      regiondesc: South America (São Paulo)
      ubuntu18: ami-05aa753c043f1dcd3
      ubuntu20: ami-054a31f1b3bf90920
  CfnHupMapping:
    CfnInitVerboseArg:
      'true': --verbose
      'false': ''
Resources:
  InstanceRole:
    Condition: CreateInstanceProfile
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: /
      Policies:
      - PolicyName: !Sub ${AWS::StackName}-taginstancepolicy
        PolicyDocument:
          Version: 2012-10-17
          Statement:
          - Effect: Allow
            Action:
            - ec2:Describe*
            - ec2:CreateTags
            Resource: '*'
      - PolicyName: !Sub ${AWS::StackName}-cfn-init
        PolicyDocument:
          Version: 2012-10-17
          Statement:
          - Effect: Allow
            Action:
            # https://stackoverflow.com/questions/29393675/does-aws-cfn-init-need-a-profile-role-for-describestackresource
            - cloudformation:DescribeStackResource
            Resource:
            - !Sub arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/${AWS::StackName}/*
  InstanceProfile:
    Condition: CreateInstanceProfile
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles:
      - !Ref InstanceRole
  LaunchConfig:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      InstanceType: !Ref InstanceType
      ImageId: !If
      - UseInstanceImageId
      - !Ref InstanceImageId
      - !FindInMap
        - AWSRegion2AMI
        - !Ref AWS::Region
        - !Ref InstanceOsVersion
      InstanceMonitoring: !If
      - UseCustomMonitoring
      - !Ref Monitoring
      - !Ref AWS::NoValue
      BlockDeviceMappings: !If
      - UseRootVolumeSize
      - - DeviceName: !Ref RootDeviceName
          Ebs:
            VolumeSize: !Ref RootVolumeSize
      - !Ref AWS::NoValue
      IamInstanceProfile: !If
      - UseExternalInstanceProfile
      - !Ref IamInstanceProfileArn
      - !If
        - CreateInstanceProfile
        - !GetAtt InstanceProfile.Arn
        - !Ref AWS::NoValue
      AssociatePublicIpAddress: !If
      - UseCustomAssociatePublicIpAddress
      - !If
        - UseAssociatePublicIpAddress
        - true
        - false
      - !Ref AWS::NoValue
      SecurityGroups: !If
      - UseCustomAssociatePublicIpAddress
      - !Ref AWS::NoValue
      - !If
        - UseCustomSecurityGroups
        - !Ref SecurityGroups
        - !Ref AWS::NoValue
      KeyName: !If
      - AssignInstanceKeyName
      - !Ref KeyName
      - !Ref AWS::NoValue
      UserData:
       Fn::If:
       - UseSignalAcceptor
       - !Base64
        Fn::Sub:
        - |
          #!/bin/sh
          set -eux
          INSTALL_PIP=''
          if which python3; then
            ! python3 -c 'import pip' && INSTALL_PIP=1
          else
            INSTALL_PIP=1
          fi
          if [ -n "$INSTALL_PIP" ]; then
            apt-get update && apt-get install -y python3-pip
          fi
          python3 -m pip \
            --disable-pip-version-check \
            install \
            --upgrade \
            https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-py3-latest.tar.gz
          INSTANCE_ROLE="$(curl http://169.254.169.254/latest/meta-data/iam/security-credentials)"
          init_exit_code=0
          /usr/local/bin/cfn-init \
            --verbose \
            --role "$INSTANCE_ROLE" \
            --region "${AWS::Region}" \
            --stack "${AWS::StackName}" \
            --resource LaunchConfig \
            --configsets create \
            || init_exit_code=$?
          /usr/local/bin/cfn-signal \
            --exit-code $init_exit_code \
            --region "${AWS::Region}" \
            --stack "${SignalAcceptorStackName}" \
            --resource "${SignalAcceptorResource}"
        - SignalAcceptorStackName: !Select
          - 0
          - !Split
            - '.'
            - !Ref SignalAcceptor
          SignalAcceptorResource: !Select
          - 1
          - !Split
            - '.'
            - !Ref SignalAcceptor
       - !Base64
        Fn::Sub: |
          #!/bin/sh
          set -eux
          INSTALL_PIP=''
          if which python3; then
            ! python3 -c 'import pip' && INSTALL_PIP=1
          else
            INSTALL_PIP=1
          fi
          if [ -n "$INSTALL_PIP" ]; then
            apt-get update && apt-get install -y python3-pip
          fi
          python3 -m pip \
            --disable-pip-version-check \
            install \
            --upgrade \
            https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-py3-latest.tar.gz
          INSTANCE_ROLE="$(curl http://169.254.169.254/latest/meta-data/iam/security-credentials)"
          exec /usr/local/bin/cfn-init \
            --verbose \
            --role "$INSTANCE_ROLE" \
            --region "${AWS::Region}" \
            --stack "${AWS::StackName}" \
            --resource LaunchConfig \
            --configsets create
    Metadata:
      AWS::CloudFormation::Init:
        configSets:
          create:
          - authorized_keys
          - keep_alive_time
          - tagging
          - ssh-username-aliases
          - amazon-time-sync-service
          - custom-init-script
          - custom-initupdate-script
          - cfn-hup-prepare
          - cfn-hup-create
          update:
          - authorized_keys
          - keep_alive_time
          - tagging
          - ssh-username-aliases
          - amazon-time-sync-service
          - custom-update-script
          - custom-initupdate-script
          - dist-upgrade
          - cfn-hup-prepare
          - cfn-hup-update
          custom-update:
          - custom-update-script
          - custom-initupdate-script
        authorized_keys:
          packages:
            apt: !If
            - UseAdditionalAuthorizedKeys
            - python3: []
            - {}
          files: !If
          - UseAdditionalAuthorizedKeys
          - /tmp/additional_authorized_keys:
              content: !Ref AdditionalAuthorizedKeys
          - {}
          commands:
            update_authorized_keys:
              test: !If
              - UseAdditionalAuthorizedKeys
              - !Sub |
                exec python3 << EOF
                from contextlib import redirect_stdout, suppress
                from pwd import getpwuid
                import os, sys
                from traceback import print_exc
                try:
                  if not os.path.exists('/tmp/additional_authorized_keys'):
                    print("Cloudformation doesn't need to update authorized_keys file because file not found. It's acceptable behaviour)))")
                    with suppress(FileNotFoundError):
                      os.remove('/tmp/additional_authorized_keys.bak')
                    sys.exit(1)
                  authorized_keys_path = os.path_join(getpwuid(1000).pw_dir, '.ssh', 'authorized_keys')
                  instance_key_name = '${KeyName}'
                  with open(authorized_keys_path, 'rt') as f:
                    actual = set(
                      key_line.strip()
                      for key_line in f
                      if len(key_line.strip().split()) < 3
                        or key_line.strip().split()[2] != instance_key_name
                    )
                  with open('/tmp/additional_authorized_keys', 'rt') as f:
                    expected = set(key_line.strip() for key_line in f)
                  if expected == actual:
                    print("Cloudformation doesn't need to update authorized_keys file because files are identical. It's acceptable behaviour)))")
                    os.remove('/tmp/additional_authorized_keys')
                    with suppress(FileNotFoundError):
                      os.remove('/tmp/additional_authorized_keys.bak')
                    sys.exit(1)
                except BaseException as er:  # NOTE: inverting return codes: 0 <=> 1
                  if isinstance(er, (KeyboardInterrupt, SystemExit)):
                    raise
                  with redirect_stdout(sys.stderr):
                    print_exc()
                EOF
              - exit 1
              command: !Sub |
                exec python3 << EOF
                from contextlib import suppress
                import os
                from pwd import getpwuid
                authorized_keys_path = os.path_join(getpwuid(1000).pw_dir, '.ssh', 'authorized_keys')
                instance_key, instance_key_name = None, '${KeyName}'
                if instance_key_name:
                  with open(authorized_keys_path, 'rt') as f:
                    [instance_key] = [
                      key_line.strip()
                      for key_line in f
                      if len(key_line.strip().split()) == 3
                        and key_line.strip().split()[2] == instance_key_name
                    ]
                with open('/tmp/additional_authorized_keys', 'rt') as r:
                  addtitional_keys = r.read()
                  if addtitional_keys[-1] != '\n':
                    addtitional_keys += '\n'
                with open(authorized_keys_path, 'wt') as w:
                  if instance_key:
                    w.write('{}\n'.format(instance_key))
                  w.write(addtitional_keys)
                os.remove('/tmp/additional_authorized_keys')
                with suppress(FileNotFoundError):
                  os.remove('/tmp/additional_authorized_keys.bak')
                EOF
        keep_alive_time:
          packages:
            apt: !If
            - UseKeepAliveTime
            - python3: []
            - {}
          commands:
            update_authorized_keys:
              test: !If
              - UseKeepAliveTime
              - !Sub |
                exec python3 << EOF
                import re
                from subprocess import run
                import sys
                expected_time_str = str(${KeepAliveTime})
                out = run(['sysctl',  'net.ipv4.tcp_keepalive_time'], capture_output=True).stdout.decode().strip()
                actual_time_str = re.match(r'net\.ipv4\.tcp_keepalive_time = (\d+)$', out).group(1)
                if expected_time_str == actual_time_str
                  print("Cloudformation doesn't need change keep alive time. It's acceptable behaviour)))")
                  sys.exit(1)
                EOF
              - exit 1
              command: !Sub |
                exec python3 << EOF
                from subprocess import run
                run(['sysctl', '-w', 'net.ipv4.tcp_keepalive_time=${KeepAliveTime}'])
                EOF
        tagging:
          packages:
            apt: !If
            - UseTags
            - python3-boto3: []
              python3-requests: []
            - {}
          commands:
            tagging:
              test: !If
              - UseTags
              - !Sub |
                exec python3 << EOF
                import boto3, requests, sys
                from contextlib import redirect_stdout
                from traceback import print_exc
                response = requests.get('http://169.254.169.254/latest/meta-data/instance-id')
                response.raise_for_status()
                instance_id = response.text
                def parse_resource_tags(comma_delimited_tags):
                  tags = []
                  if comma_delimited_tags:
                    for tag in comma_delimited_tags.split(','):
                      key, value = tag.split('=', maxsplit=1)
                      tags.append({'Key': key, 'Value': value})
                  return tags
                expected_tags = {
                  'instance': parse_resource_tags('${InstanceTags}'),
                  'root_volume': parse_resource_tags('${RootVolumeTags}'),
                }
                try:
                  ec2 = boto3.session.Session(region_name='${AWS::Region}').resource('ec2')
                  instance = next(iter(ec2.instances.filter(InstanceIds=[instance_id])))
                  [root_volume] = [
                    volume
                    for volume in instance.volumes.filter(MaxResults=5)
                    if '${RootDeviceName}' in (
                      attach['Device']
                      for attach in volume.attachments
                    )
                  ]
                  actual_tags = {
                    resource: [
                      tag
                      for tag in globals()[resource].tags or []
                      if tag['Key'] in [t['Key'] for t in expected_tags[resource]]
                    ] for resource in expected_tags
                  }
                  if expected_tags == actual_tags:
                    _actual_tags = {
                      resource: {
                        tag['Key']: tag['Value']
                        for tag in actual_tags[resource]
                      } for resource in expected_tags
                    }
                    print("Cloudformation doesn't need to update any tags. It's acceptable behaviour)))")
                    print(_actual_tags)
                    sys.exit(1)
                except BaseException as er:  # NOTE: inverting return codes: 0 <=> 1
                  if isinstance(er, (KeyboardInterrupt, SystemExit)):
                    raise
                  with redirect_stdout(sys.stderr):
                    print_exc()
                EOF
              - exit 1
              command: !Sub |
                exec python3 << EOF
                import boto3, requests, sys
                from botocore.exceptions import NoCredentialsError
                from contextlib import redirect_stdout
                from traceback import print_exc
                response = requests.get('http://169.254.169.254/latest/meta-data/instance-id')
                response.raise_for_status()
                instance_id = response.text
                def parse_resource_tags(comma_delimited_tags):
                  tags = []
                  if comma_delimited_tags:
                    for tag in comma_delimited_tags.split(','):
                      key, value = tag.split('=', maxsplit=1)
                      tags.append({'Key': key, 'Value': value})
                  return tags
                expected_tags = {
                  'instance': parse_resource_tags('${InstanceTags}'),
                  'root_volume': parse_resource_tags('${RootVolumeTags}'),
                }
                try:
                  ec2 = boto3.session.Session(region_name='${AWS::Region}').resource('ec2')
                  instance = next(iter(ec2.instances.filter(InstanceIds=[instance_id])))
                  [root_volume] = [
                    volume
                    for volume in instance.volumes.filter(MaxResults=5)
                    if '${RootDeviceName}' in ( 
                      attach['Device']
                      for attach in volume.attachments
                    ) 
                  ]
                  actual_tags = {
                    resource: [
                      tag
                      for tag in globals()[resource].tags or []
                      if tag['Key'] in [t['Key'] for t in expected_tags[resource]]
                    ] for resource in expected_tags
                  }
                  for resource in expected_tags:
                    if expected_tags[resource] != actual_tags[resource]:
                      _actual_tags = {tag['Key']: tag['Value'] for tag in actual_tags[resource]}
                      _updating_tags = ', '.join(
                        f"{tag['Key']} from {_actual_tags.get(tag['Key'])} to {tag['Value']}"
                        for tag in expected_tags[resource]
                      )
                      print(f'Updating {resource} tags: {_updating_tags}')
                      globals()[resource].create_tags(Tags=expected_tags[resource])
                except NoCredentialsError:
                  with redirect_stdout(sys.stderr):
                    print_exc()
                    print()
                    print(
                      'You should assign IamInstanceProfile with tag permissions'
                      ' (https://aws.amazon.com/premiumsupport/knowledge-center'
                      '/cloudformation-instance-tag-root-volume/)'
                    )
                EOF
        ssh-username-aliases:
          packages:
            apt: !If
            - UseSshUsernameAliases
            - python3: []
            - {}
          commands:
            ssh-username-aliases:
              test: !If
              - UseSshUsernameAliases
              -  !Sub |
                exec python3 << EOF
                from pwd import getpwall, getpwuid
                import sys
                default_user_pwd = getpwuid(1000)
                actual_aliases = {
                    u.pw_name
                    for u in getpwall()
                    if u.pw_uid == default_user_pwd.pw_uid and u.pw_name != default_user_pwd.pw_name
                }
                expected_aliases = set('${SshUsernameAliases}'.split(','))
                if expected_aliases == actual_aliases:
                    print("Cloudformation doesn't need to update ssh username aliases. It's acceptable behaviour)))")
                    sys.exit(1)
                EOF
              - exit 1
              command: !Sub |
                exec python3 << EOF
                from pwd import getpwall, getpwuid
                import subprocess
                default_user_pwd = getpwuid(1000)
                actual_aliases = {
                    u.pw_name
                    for u in getpwall()
                    if u.pw_uid == default_user_pwd.pw_uid and u.pw_name != default_user_pwd.pw_name
                }
                expected_aliases = set('${SshUsernameAliases}'.split(','))
                for alias in expected_aliases - actual_aliases:
                    subprocess.run([
                        'useradd',
                        '--non-unique',
                        '--uid', str(default_user_pwd.pw_uid),
                        '--gid', str(default_user_pwd.pw_gid),
                        '--home', default_user_pwd.pw_dir,
                        '--shell', default_user_pwd.pw_shell,
                        '--comment', f'{default_user_pwd.pw_name} alias',
                        alias,
                    ])
                for alias in actual_aliases - expected_aliases:
                    # NOTE: --force because uid may be blocked as default user
                    subprocess.run(['userdel', '--force', alias])
                EOF
        amazon-time-sync-service:
          packages:
            apt: !If
            - UseActivateAmazonTimeSyncService
            - chrony: []
            - {}
          commands:
            amazon-time-sync-service:
              test: !If
              - UseActivateAmazonTimeSyncService
              - |
                set -eux
                [ -f /etc/chrony/chrony.conf ] || exit 0
                grep ^pool\  /etc/chrony/chrony.conf && exit 0
                if grep ^server\  /etc/chrony/chrony.conf; then
                  if grep ^server\ 169\.254\.169\.123 /etc/chrony/chrony.conf; then
                    echo "Amazon Time Sync service doesn't need to update configs. It's acceptable behaviour)))"
                    exit 1
                  fi
                fi
                exit 0
              - |
                echo "Amazon Time Sync service disabled. It's acceptable behaviour)))"
                exit 1
              command: |
                set -eux
                sed -i -e's/^pool /# pool/' /etc/chrony/chrony.conf
                if grep ^server\  /etc/chrony/chrony.conf; then
                  sed -i -e'/^server 169\.254\.169\.123/! s/^\(server \([[:digit:]]\)\{1,3\}\(\.\([[:digit:]]\)\{1,3\}\)\)/# \1/' /etc/chrony/chrony.conf
                  sed -i -e's/^# \(server 169\.254\.169\.123\)/\1/' /etc/chrony/chrony.conf
                else
                  echo >> /etc/chrony/chrony.conf
                  echo '# Amazon Time Sync Service' >> /etc/chrony/chrony.conf
                  echo 'server 169.254.169.123 prefer iburst minpoll 4 maxpoll 4' >> /etc/chrony/chrony.conf
                fi
                systemctl restart chrony
        custom-init-script:
          commands:
            custom-init-script:
              command: !Ref CustomInitScript
        custom-update-script:
          commands:
            custom-update-script:
              command: !Ref CustomUpdateScript
        custom-initupdate-script:
          commands:
            custom-initupdate-script:
              command: !Ref CustomInitUpdateScript
        dist-upgrade:
          commands:
            apt_dist-upgrade:
              command: |
                set -eux
                export DEBIAN_FRONTEND=noninteractive
                apt-get update
                apt-get dist-upgrade -y
                apt-get autoremove -y
        cfn-hup-prepare:
          packages:
            apt: 
              python3-pip: []
          files:
            /etc/cfn/cfn-hup.conf:
              content: !Sub |
                [main]
                verbose=${CfnHupVerbose}
                # NOTE: region should be exist, if not will fail
                region=${AWS::Region}
                stack=${AWS::StackId}
                interval=5  # will be ignored, use cfn-hup.timer
              mode: '000400'
              owner: root
              group: root
            /etc/cfn/hooks.d/cfn-auto-reloader.conf:
              content: !Sub
              - |
                [cfn-auto-reloader-hook]
                triggers=post.update
                path=Resources.LaunchConfig.Metadata.AWS::CloudFormation::Init
                action=/usr/local/bin/cfn-init \
                         ${CfnInitVerboseArg} \
                         --role "{{ INSTANCE_ROLE }}" \
                         --region "${AWS::Region}" \
                         --stack "${AWS::StackId}" \
                         --resource LaunchConfig \
                         --configsets update
                runas=root
              - CfnInitVerboseArg: !FindInMap
                - CfnHupMapping
                - CfnInitVerboseArg
                - !Ref CfnHupVerbose
              mode: '000400'
              owner: root
              group: root
            /etc/systemd/system/cfn-hup.service:
              content: |
                [Unit]
                Description=cfn-hup executor
                [Service]
                Type=simple
                ExecStart=/usr/local/bin/cfn-hup --no-daemon
              mode: '000644'
              owner: root
              group: root
            /etc/systemd/system/cfn-hup.timer:
              content: !Sub |
                [Unit]
                Description=Periodically running cfn-hup
                [Timer]
                OnBootSec=0min
                OnUnitActiveSec=${CfnHupTimer}
                [Install]
                WantedBy=timers.target
              mode: '000644'
              owner: root
              group: root
          commands:
            replace_instance_role:
              command: |
                export INSTANCE_ROLE="$(curl http://169.254.169.254/latest/meta-data/iam/security-credentials)"
                python3 -m pip install j2cli
                mv -v /etc/cfn/hooks.d/cfn-auto-reloader.conf /tmp/cfn-auto-reloader.conf.j2
                j2 /tmp/cfn-auto-reloader.conf.j2 > /etc/cfn/hooks.d/cfn-auto-reloader.conf
        cfn-hup-create:
          commands:
            start_cfn-hup:
              command: |
                set -eux
                systemctl daemon-reload
                systemctl enable cfn-hup.timer
                systemctl start cfn-hup.timer
        cfn-hup-update:
          commands:
            daemon-reload_cfn-hup:
              command: |
                set -eux
                should_reload=''
                for resource in cfn-hup.service cfn-hup.timer; do
                  exit_code=0
                  systemctl status $resource 2>&1 | grep "'systemctl daemon-reload'" || exit_code=$?
                  if [ $exit_code -eq 0 ]; then
                    if [ -f $resource.bak ]; then
                      diff -u $resource $resource.bak && continue
                    fi
                    should_reload=1
                    break
                  else
                    echo "\`systemctl status $resource | grep\` finished with exit code $exit_code"
                    [ $exit_code -eq 1 ] || exit 1
                  fi
                done
                if [ -n "$should_reload" ]; then
                  systemctl daemon-reload
                  systemctl --no-block restart cfn-hup.timer
                fi
Outputs:
  LaunchConfig:
    Value: !Ref LaunchConfig
